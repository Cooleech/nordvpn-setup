NordVPN setup je skripta za instalaciju NordVPN-a na .deb i .rpm bazirane distre.
Testirana je samo na Linux Mintu 18 (thnx, JohnDoe!)
Skripta traži samo zenity paket, ostalo sama instalira

Skriptu možete pokrenuti tako da u terminalu ukucate:

bash /putanja/do/skripte/NordVPN_setup.sh

..gdje je /putanja/do/skripte/ stvarna putanja do skripte (npr. $HOME/Downloads/)

Ako primjetite kakve probleme prilikom instalacije, možete me kontaktirati na:

cooleechNAgmailTOČKAcom