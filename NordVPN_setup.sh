#!/bin/bash
########################
#  NordVPN setup v0.1  #
# by Cooleech (c) 2020 #
########################
# NAPOMENA: Skripta radi samo na distrama koje podržavaju .deb ili .rpm pakete!

NASLOV="NordVPN setup"

LOZINKA_KORISNIKA () {
LOZINKA=`zenity --entry --hide-text --width 385 --title "$NASLOV" --text "Upiši lozinku korisnika $(whoami):" --cancel-label "Izlaz" --ok-label "U redu"`
if [ "$LOZINKA" = "" ]; then
 exit 1
fi
echo "$LOZINKA" | sudo -S echo "test"
if [ $? != 0 ]; then
 zenity --error --width 150 --title "$NASLOV" --text "Pogrešna lozinka?" --timeout 2
 LOZINKA_KORISNIKA
fi
}

# Uvezi postavke korisničkih mapa
. ~/.config/user-dirs.dirs

# Otkrij paketni manager i podesi varijable
which yum
if [ $? = 0 ]; then
 LINK="https://repo.nordvpn.com/yum/nordvpn/centos/noarch/Packages/n/nordvpn-release-1.0.0-1.noarch.rpm"
 PACKAGE_MANAGER="yum"
 UPDATE="check-update"
fi
which dnf
if [ $? = 0 ]; then
 LINK="https://repo.nordvpn.com/yum/nordvpn/centos/noarch/Packages/n/nordvpn-release-1.0.0-1.noarch.rpm"
 PACKAGE_MANAGER="dnf"
 UPDATE="check-update"
fi

which apt-get
if [ $? = 0 ]; then
 LINK="https://repo.nordvpn.com/deb/nordvpn/debian/pool/main/nordvpn-release_1.0.0_all.deb"
 PACKAGE_MANAGER="apt-get"
 UPDATE="update"
fi
which apt
if [ $? = 0 ]; then
 LINK="https://repo.nordvpn.com/deb/nordvpn/debian/pool/main/nordvpn-release_1.0.0_all.deb"
 PACKAGE_MANAGER="apt"
 UPDATE="update"
fi

if [ "$PACKAGE_MANAGER" = "" ]; then
 zenity --error --width 288 --title "$NASLOV - kuku lele ;-(" --text "Nažalost, vaša distribucija nije podržana!\n\nSkripta radi samo s deb i rpm paketima!"
 exit 1
fi

# Preuzmi odgovarajući paket i ikonu za desktop
which wget
if [ $? = 0 ]; then
 (
 echo "# Preuzimam NordVPN paket..."
 wget $LINK -O "$HOME/${LINK##*/}"
 if [ $? != 0 ]; then
  zenity --error --width 150 --title "$NASLOV" --text "Neuspjeh pri preuzimanju paketa."
  exit 1
 fi
 ) | zenity --width 360 --progress --title "$NASLOV" --percentage 0 --auto-close --no-cancel --pulsate
 mkdir $HOME/.icons
 (
 echo "# Preuzimam ikonu za NordVPN..."
 wget https://s1.nordcdn.com/nordvpn/media/1.170.0/images/global/favicon/favicon-128.png -O $HOME/.icons/NordVPN-128.png
 if [ $? != 0 ]; then
  zenity --error --width 150 --title "$NASLOV" --text "Neuspjeh pri preuzimanju ikone."
 fi
 ) | zenity --width 360 --progress --title "$NASLOV" --percentage 0 --auto-close --no-cancel --pulsate
else
 (
 echo "# Preuzimam NordVPN paket..."
 curl -S $LINK -o "$HOME/${LINK##*/}"
 if [ $? != 0 ]; then
  zenity --error --width 150 --title "$NASLOV" --text "Neuspjeh pri preuzimanju paketa."
  exit 1
 fi
 ) | zenity --width 360 --progress --title "$NASLOV" --percentage 0 --auto-close --no-cancel --pulsate
 mkdir $HOME/.icons
 (
 echo "# Preuzimam ikonu za NordVPN..."
 curl -S https://s1.nordcdn.com/nordvpn/media/1.170.0/images/global/favicon/favicon-128.png -o $HOME/.icons/NordVPN-128.png
 if [ $? != 0 ]; then
  zenity --error --width 150 --title "$NASLOV" --text "Neuspjeh pri preuzimanju ikone."
 fi
 ) | zenity --width 360 --progress --title "$NASLOV" --percentage 0 --auto-close --no-cancel --pulsate
fi

# Pozovi funkciju
LOZINKA_KORISNIKA

# Instaliraj paket
(
echo "# Instalacija NordVPN paketa u tijeku..."
echo "$LOZINKA" | sudo -S $PACKAGE_MANAGER -y install "$HOME/${LINK##*/}"
) | zenity --width 360 --progress --title "$NASLOV" --percentage 0 --auto-close --no-cancel --pulsate

# Osvježi repozitorije
(
echo "# Osvježavam repozitorije..."
echo "$LOZINKA" | sudo -S $PACKAGE_MANAGER -y $UPDATE
) | zenity --width 360 --progress --title "$NASLOV" --percentage 0 --auto-close --no-cancel --pulsate

# Instaliraj NordVPN
(
echo "# Instaliram NordVPN klijent..."
echo "$LOZINKA" | sudo -S $PACKAGE_MANAGER -y install nordvpn
) | zenity --width 360 --progress --title "$NASLOV" --percentage 0 --auto-close --no-cancel --pulsate

# Napravi desktop shortcut za spajanje
echo -e "[Desktop Entry]\nName=NordVPN\nComment=Spoji se na NordVPN\nExec=nordvpn connect\nTerminal=true
Type=Application\nStartupNotify=true\nIcon=$HOME/.icons/NordVPN-128.png" > "$XDG_DESKTOP_DIR"/NordVPN.desktop
chmod 700 "$XDG_DESKTOP_DIR"/NordVPN.desktop

# To je to
zenity --info --width 300 --title "$NASLOV" --text "Instalacija NordVPN-a je gotova!\n
Prije prvog spajanja, u terminalu upišite naredbu:\n\n<i>nordvpn login</i>\n
te unesite svoje podatke za spajanje. Nakon toga, upišite:\n\n<i>nordvpn connect</i>\n
ili kliknite na ikonu na vašem desktopu za spajanje na NordVPN."
exit